package com.example.securingweb;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PreAuthorize("hasRole('admin')")
public class TestController {
    @GetMapping("/test")
    @PreAuthorize("hasRole('admin')")
    public String test(){
        return "test";
    }

    @GetMapping("/test2")
    public String test2(){
        return "test2";
    }

    @GetMapping("/shichang")
    @PreAuthorize("hasRole('admin') or hasRole('shichang')")
    public String shichang(){
        return "shichang";
    }

    @GetMapping("/xingzheng")
    @PreAuthorize("hasRole('admin') or hasRole('xingzheng')")
    public String xingzheng(){
        return "xingzheng";
    }

    @GetMapping("/caiwu")
    @PreAuthorize("hasRole('admin') or hasRole('caiwu')")
    public String caiwu(){
        return "caiwu";
    }
}
