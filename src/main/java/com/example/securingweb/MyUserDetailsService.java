package com.example.securingweb;

import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Map<String,String> users = new HashMap<>();
        users.put("user1","123");
        users.put("user2","123");
        users.put("user3","123");
        users.put("user4","123");
        users.put("user5","123");

        Map<String,String[]> roles = new HashMap<>();
        roles.put("user1",new String[]{"admin"});
        roles.put("user2",new String[]{"caiwu"});
        roles.put("user3",new String[]{"xingzheng"});
        roles.put("user4",new String[]{"shichang"});
        roles.put("user5",new String[]{"shichang","caiwu"});

        System.out.println(s);
        System.out.println(users.get(s));



        UserDetails admin =  User.withDefaultPasswordEncoder()
                .username(s)
                .password(users.get(s))
                .roles(roles.get(s))
                .build();

        System.out.println(roles.get(s));
        // 返回用户对象
        return admin;
    }
}
